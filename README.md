# Lumberjack

## General
For all input formats (Fasta, Newick and Phylip) use plain ID names.

## Folders

### binary

**lumberjack** contains all libraries and works on MacOS or Linux system

MacOS-version: lumberjack-MacOS
Linux-version: lumberjack-Linux

### data
* **datasets** set1, set2, set3 are the raw datasets
* **results** evaluation.pdf which includes the analysis 

### docs

lumberjack_source.txt contains the definitions of the Perl functions

### examples

three different use cases are described and can be applied by the user

### source

contains source code written in Perl


## Usage

    ./lumberjack -h

    arguments:
        -f fasta file or path to fasta file(s)
        -n newick file or path to newick file(s)
        -d distance file or path to distance matrix file(s)
        -o path for results (default: ./lumberjack_result/)
        -t threshold to detect inconsistent edges (default: 2)

## Examples (see Examples in cyanolab-uni-freiburg/Lumberjack/)

### Fasta (examples/use_case_fasta)
* Input (my.fasta):  

    \>_000911     
    ATAAGGCTATGGAAACCCGACAGAATTCCAAAGCATGAACTATTAAGGG
    CACGGGAACTAGGGGAAACGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT  
    \>_000912  
    ATAAGGCTATGGAAACCCGACAGAATTCCCCCGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGCCCCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGAAATCGGTGATGCCGGGGTATTTTTT  
    \>_000913  
    ATAAGGCTATGGAAAGGGGACAGAATTCCGGGGCATGAACTAGGAAGGG
    CACGGGTTCTAGGGGTTTCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGGGGTCGGTGATGCCGGGGTATTTTTT  
    \>_000914  
    ATAAGGCTATGGAAAGGGGACAGAATTCCGGGGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGGGGCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT  
    \>_000915  
    ATAAGGCTATGGAAACCCGACAGAATTCCTTTGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGTTTCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT  
    \>_000916  
    ATAAGGCTATGGAAACCCGACAGAATTCCTTTGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGTTTCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT  

    

* Invocation:  

    ./lumberjack -f ./my.fasta 

* Output (./lumberjack_result/):
  * Cluster 1 (cluster-1of2-my.fasta):

    \>_000915  
    ATAAGGCTATGGAAACCCGACAGAATTCCTTTGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGTTTCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT  
    \>_000916  
    ATAAGGCTATGGAAACCCGACAGAATTCCTTTGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGTTTCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT  

  * Cluster 2 (cluster-2of2-my.fasta):

    \>_000911     
    ATAAGGCTATGGAAACCCGACAGAATTCCAAAGCATGAACTATTAAGGG
    CACGGGAACTAGGGGAAACGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT  
    \>_000912  
    ATAAGGCTATGGAAACCCGACAGAATTCCCCCGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGCCCCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGAAATCGGTGATGCCGGGGTATTTTTT  
    \>_000913  
    ATAAGGCTATGGAAAGGGGACAGAATTCCGGGGCATGAACTAGGAAGGG
    CACGGGTTCTAGGGGTTTCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGGGGTCGGTGATGCCGGGGTATTTTTT  
    \>_000914  
    ATAAGGCTATGGAAAGGGGACAGAATTCCGGGGCATGAACTATTAAGGG
    CACGGGTTCTAGGGGGGGCGGTACCTTCCATTGACATTGAGTTCTCCTA
    GATATGCCCTCGGTGATGCCGGGGTATTTTTT

### Newick (examples/use_case_newick)
* Input:  
    
    (>A:0.00000,>B:0.00000,(>C:0.00000,(>D:0.00000,(>E:7.90470,>F:7.97451):0.74836):0.00000):0.00000);

* Invocation:  
 
    ./lumberjack -n ./my.newick

* Output (./lumberjack_result/lumberjack_my.newick):

    cluster 1: >D;>A;>C;>B  
    cluster 2: >F;>E


### Distance matrix - phylip format (examples/use_case_distance_matrix)
* Input (my.phylip):  

   6  
    \>A    0 -3.41505e-07 -3.41505e-07 -3.41505e-07 8.65305 8.72286  
    \>B    -3.41505e-07 0 -3.41505e-07 -3.41505e-07 8.65305 8.72286  
    \>C    -3.41505e-07 -3.41505e-07 0 -3.41505e-07 8.65305 8.72286  
    \>D    -3.41505e-07 -3.41505e-07 -3.41505e-07 0 8.65305 8.72286  
    \>E    8.65305 8.65305 8.65305 8.65305 0 15.8792  
    \>F    8.72286 8.72286 8.72286 8.72286 15.8792 0  


* Invocation:  

    ./lumberjack -d ./my.phylip

* Output (./lumberjack_result/lumberjack_my.phylip):

    cluster 1: >D;>A;>B;>C  
    cluster 2: >F;>E

